import java.util.Scanner;

public class OX {
    static char table[][] = {
            {'_','_','_'},
            {'_','_','_'},
            {'_','_','_'}
    };
    static int row, col;
    static char player = 'X';
    static int turn = 0;

    public static void main(String[] args) {
        printWelcomeMessage();
        while(true) {
            printTable();
            printPlayerTurn();
            input();
            if(checkWin()) {
                break;
            }
            switchPlayer();
        }
        printTable();
        printMatchResult();
        printCongratulations();
    }



    private static void printMatchResult() {
        if(checkDraw()){
            System.out.println("Draw");
        }else {
            System.out.println("Player " + player + " Win");
        }
    }

    private static void switchPlayer() {
        if(player =='X'){
            player = 'O';
        } else {
            player = 'X';
        }
        turn++;
        System.out.println(turn);
    }

    private static boolean checkWin() {
        if(checkRow()) {
            return true;
        }
        if(checkCol()) {
            return true;
        }
        if(checkDiagonal()){
            return true;
        }
        if(checkDraw()){
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        if(turn == 8){
            return true;
        }
        return false;
    }

    private static boolean checkDiagonal() {
        if(checkDiagonal1()){
            return true;
        }
        if(checkDiagonal2()){
            return true;
        }
        return false;
    }

    private static boolean checkDiagonal1() {
        for(int i = 0; i < table.length; i++){
            if(table[i][i] != player)
                return false;
        }
        return  true;
    }

    private static boolean checkDiagonal2() {
        for(int i = 0; i < table.length; i++){
            if(table[i][2-i] != player)
                return false;
        }
        return  true;
    }

    private static boolean checkRow(int rowInd) {
        for(int colInd = 0; colInd < table[rowInd].length; colInd++){
            if(table[rowInd][colInd] != player)
                return false;
        }
        return true;
    }

    private static boolean checkRow() {
        for(int rowInd = 0; rowInd < table.length; rowInd++) {
            if(checkRow(rowInd))
                return true;
        }
        return false;
    }

    public static boolean checkCol(int colInd) {
        for(int rowInd = 0; rowInd < table[colInd].length; rowInd++){
            if(table[rowInd][colInd] != player)
                return false;
        }
        return true;
    }

    public static boolean checkCol() {
        for(int colInd = 0; colInd < table[0].length; colInd++){
            if(checkCol(colInd))
                return true;
        }
        return false;
    }


    private static void input() {
        while(true) {
            try {
                Scanner kb = new Scanner(System.in);
                System.out.println("Please input Row Col: ");
                String input = kb.nextLine();
                String str[] = input.split(" ");
                if (str.length != 2) {
                    System.out.println("Error: Please input Row and Col");
                    continue;
                }

                row = Integer.parseInt(str[0]);
                col = Integer.parseInt(str[1]);
                if(row > 3 || row < 1 || col > 3 || col < 1){
                    System.out.println("Error: Please input between 1-3");
                    continue;
                }
                if(!setTable()){
                    System.out.println("Error: Please input another Row and Col");
                    continue;
                }
                break;
            }catch (Exception e) {
                System.out.println("Error: Please input Number");
                continue;
            }

        }

    }

    private static boolean setTable() {
        if(table[row-1][col-1] != '_'){
            return false;
        }
        table[row-1][col-1] = player;
        return true;
    }

    private static void printPlayerTurn() {
        System.out.println(player+" Turn");
    }

    private static void printTable() {
        System.out.println("  1 2 3");
        for(int rowInd = 0; rowInd < table.length; rowInd++){
            System.out.print(rowInd+1);
            for(int colInd = 0; colInd < table[rowInd].length; colInd++) {
                System.out.print(" " + table[rowInd][colInd]);
            }
            System.out.println();
        }
    }

    private static void printWelcomeMessage() {
        System.out.println("Welcome to OX Game");
    }

    private static void printCongratulations() {
        if(checkDraw()){
            System.out.println("!!!");
        }else {
            System.out.println("Congratulations");
        }
    }
}
